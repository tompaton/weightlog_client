import csv
import traceback
from collections import namedtuple

import requests


class PostError(Exception):
    pass


class WeightLog:

    WEIGHT = 'https://weightlog.tompaton.com/'

    def __init__(self, api_token, title, unit):
        self._api_token = api_token
        self.title = title
        self.unit = unit

    def get_url(self, *path):
        return '{}{}'.format(self.WEIGHT,
                             '/'.join([self._api_token] + list(path)))

    Reading = namedtuple('Reading', 'date reading unit')

    def get_readings(self, num=10):
        log_csv = self._send_request('GET', self.get_url('csv')).text
        rows = list(csv.reader(log_csv.split('\n'), delimiter=','))[1:]
        return [self.Reading(row[0], row[1], self.unit)
                for row in rows[-(num + 1):]
                if row]

    def log_reading(self, when, reading):

        # oops, no security on this, can just POST directly to the log once we
        # get the crsftoken cookie! should have to provide the "edit" password
        # if the log is "locked".

        cookies = self._send_request('GET', self.get_url()).cookies
        data = {'reading{}'.format(when.strftime('%Y%m%d')): reading}
        res = self._send_request('POST', self.get_url('readings'),
                                 cookies=cookies,
                                 headers={'X-CSRFToken': cookies['csrftoken']},
                                 data=data)

        if res.status_code == 200:
            return self.Reading(when.strftime('%Y-%m-%d'), reading, self.unit)

        else:
            raise PostError(res.text)

    def _send_request(self, *args, **kwargs):  # pragma: no cover
        req = requests.Request(*args, **kwargs)
        prepared = req.prepare()
        try:
            res = requests.Session().send(prepared)
        except:
            traceback.print_exc()
            self._pretty_print_post(prepared)
            print(res.raw.read())
        else:
            return res

    @staticmethod
    def _pretty_print_post(req):  # pragma: no cover
        """
        At this point it is completely built and ready
        to be fired; it is "prepared".

        However pay attention at the formatting used in
        this function because it is programmed to be pretty
        printed and may differ from the actual request.
        """
        print('{}\n{}\n{}\n\n{}'.format(
            '-----------START-----------',
            req.method + ' ' + req.url,
            '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
            req.body,
        ))
