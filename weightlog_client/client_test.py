from datetime import date

from unittest.mock import Mock, patch, call

import pytest

import weightlog_client as client


@pytest.fixture
def log():
    return client.WeightLog('secret-token', 'Title', 'kg')


@pytest.fixture
def log_csv():
    return ('Date,Reading,Notes\n'
            '2009-10-01,76.60,\n'
            '2009-10-02,77.60,\n'
            '2009-10-03,77.10,\n')


def test_get_url(log):
    assert log.get_url('x', 'y') \
        == 'https://weightlog.tompaton.com/secret-token/x/y'


def test_get_readings(log, log_csv):
    with patch.object(log, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.text = log_csv

        assert log.get_readings() == [
            client.WeightLog.Reading('2009-10-01', '76.60', 'kg'),
            client.WeightLog.Reading('2009-10-02', '77.60', 'kg'),
            client.WeightLog.Reading('2009-10-03', '77.10', 'kg'),
        ]

        send.assert_called_with(
            'GET', 'https://weightlog.tompaton.com/secret-token/csv')


def test_log_reading(log):
    with patch.object(log, '_send_request') as send:
        get_res = Mock()
        get_res.cookies = {'csrftoken': 'csrf'}

        post_res = Mock()
        post_res.status_code = 200

        send.side_effect = [get_res, post_res]

        assert log.log_reading(date(2009, 10, 4), 80) \
            == client.WeightLog.Reading('2009-10-04', 80, 'kg')

        assert send.mock_calls == [
            call('GET', 'https://weightlog.tompaton.com/secret-token'),
            call('POST', 'https://weightlog.tompaton.com/secret-token/readings',
                 cookies={'csrftoken': 'csrf'},
                 headers={'X-CSRFToken': 'csrf'},
                 data={'reading20091004': 80}),
        ]


def test_log_reading_error(log):
    with patch.object(log, '_send_request') as send:
        get_res = Mock()
        get_res.cookies = {'csrftoken': 'csrf'}

        post_res = Mock()
        post_res.status_code = 500
        post_res.text = 'error'

        send.side_effect = [get_res, post_res]

        with pytest.raises(client.PostError) as execinfo:
            log.log_reading(date(2009, 10, 4), 80)

        assert str(execinfo.value) == 'error'

        assert send.mock_calls == [
            call('GET', 'https://weightlog.tompaton.com/secret-token'),
            call('POST', 'https://weightlog.tompaton.com/secret-token/readings',
                 cookies={'csrftoken': 'csrf'},
                 headers={'X-CSRFToken': 'csrf'},
                 data={'reading20091004': 80}),
        ]
